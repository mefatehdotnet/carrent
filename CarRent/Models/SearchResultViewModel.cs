﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRent.Models
{
    public class SearchResultViewModel
    {
        public string ManufacturerName { get; set; }
        public string ModelName { get; set; }
        public int Year { get; set; }
        public bool GearType { get; set; }
        public string CarPic { get; set; }
        public DateTime StartRent { get; set; }
        public DateTime EndDate { get; set; }

        




    }
}