﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRent.BL;
namespace CarRent.Models
{
    public class SearchViewModel
    {
               
        public bool Gear { get; set; }

        public int YearList { get; set; }

        public decimal DailyRent { get; set; }

        //public int MenufactureID { get; set; }

        //public int ModelID { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        //public string ManufactureName { get; set; }


    }
}