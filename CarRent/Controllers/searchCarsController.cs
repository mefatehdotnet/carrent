﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarRent.BL;
using CarRent.Models;

namespace CarRent.Controllers
{
    public class searchCarsController : Controller
    {
        // GET: searchCars
       
        public ActionResult Index()
        {
            using (CarManeger logic = new CarManeger())
            {
                List<Car> carList = logic.GetAllCars().ToList();
                return PartialView(carList);   
            }
        }
        public ActionResult Search()
        {    
                return View();
        }
       
        //[HttpPost]
        public ActionResult SearchResualt(int carYear, bool carGear, DateTime startRent, DateTime endRent) 
        {
            using (CarManeger logic = new CarManeger())
            {
                List<Car> carList = logic.SearchCars(carYear, carGear, startRent, endRent).ToList();
                return PartialView(carList);    
            }
        }
      
        }
       
    }
