﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent.BL
{
    public abstract class BaseLogic : IDisposable
    {
        protected CarRentEntities2 DB = new CarRentEntities2();

        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
