//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarRent.BL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CarType
    {
        public CarType()
        {
            this.Cars = new HashSet<Car>();
        }
    
        public int CarTypeID { get; set; }
        public int ManufacturerID { get; set; }
        public int ModelID { get; set; }
        public int Year { get; set; }
        public bool Gear { get; set; }
        public decimal DailyRent { get; set; }
        public decimal DailyOverdue { get; set; }
    
        public virtual ICollection<Car> Cars { get; set; }
        public virtual Manufacture Manufacture { get; set; }
        public virtual Model Model { get; set; }
    }
}
