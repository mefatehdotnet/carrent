﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CarRent.BL
{
    public class CarManeger : BaseLogic
    {
        private CarRentEntities2 _db = new CarRentEntities2();

        public List<Car> SearchCars(int Year, bool Gear, DateTime startRent,DateTime endRent) // , int manufacturerID, DateTime ActualEndDate
        {
            var query = from z in _db.Cars
                        where z.CarType.Year == Year &&
                              z.CarType.Gear == Gear &&
                              z.RentsOrders.Max(x => x.ActualEndDate) < startRent &&
                              z.RentsOrders.Max(x => x.ActualEndDate) < endRent
                        select z;
            return query.ToList();
            //var query = from z in _db.Cars
            //            join z in _db.CarTypes on _db. Cars.CarPlateNumber  equals RentsOrders.CarPlateNumber
            //            where z.CarType.Year == Year &&
            //                  z.CarType.Gear == Gear 
                                
            //            select z;
            //return query.ToList();
           

        }

        public List<Car> GetAllCars()
        {
            var quary = from z in _db.Cars
                        select z;
            return quary.ToList();
        }

    }
}
